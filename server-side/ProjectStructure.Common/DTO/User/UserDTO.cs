﻿using System;

namespace ProjectStructure.Common.DTO.User
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? TeamId { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
    }
}