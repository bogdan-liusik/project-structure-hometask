﻿using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.Common.DTO.LinqQueryResults
{
    public class Query1StructureDTO
    {
        public ProjectDTO Project { get; set; }
        public int TasksCount { get; set; }
    }
}