﻿using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.Common.DTO.LinqQueryResults
{
    public class Query7StructureDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int TeamMembersCountByCondition { get; set; }
    }
}