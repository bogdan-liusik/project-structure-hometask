﻿using System;

namespace ProjectStructure.Common.DTO.Project
{
    public class ProjectUpdateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}