﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly IRepository<Task> _tasksRepository;
        
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _tasksRepository = _unitOfWork.Set<Task>();
        }
        
        public ICollection<TaskDTO> GetAllTasks()
        {
            return _tasksRepository.GetAll().Select(project => _mapper.Map<TaskDTO>(project)).ToList();
        }

        public TaskDTO GetTaskById(int id)
        {
            var task = _tasksRepository.Get(id);

            if (task == null)
            {
                throw new NotFoundException(typeof(Task).ToString(), id);
            }
            
            return _mapper.Map<TaskDTO>(task);
        }

        public TaskDTO CreateTask(TaskCreateDTO taskCreateDto)
        {
            var createdTask = _tasksRepository.Create(_mapper.Map<Task>(taskCreateDto));
            _unitOfWork.SaveChanges();
            return _mapper.Map<TaskDTO>(createdTask);
        }

        public TaskDTO UpdateTask(TaskUpdateDTO taskUpdateDto)
        {
            var taskExists = _tasksRepository.GetAll().Any(t => t.Id == taskUpdateDto.Id);

            if (!taskExists)
            {
                throw new NotFoundException(typeof(Task).ToString(), taskUpdateDto.Id);
            }
            
            var updatedTask = _tasksRepository.Update(_mapper.Map<Task>(taskUpdateDto));
            _unitOfWork.SaveChanges();
            
            return _mapper.Map<TaskDTO>(updatedTask);
        }

        public void DeleteTask(int taskId)
        {
            if (_tasksRepository.Get(taskId) == null)
            {
                throw new NotFoundException(typeof(Task).ToString(), taskId);
            }
            
            _tasksRepository.Delete(taskId);
            _unitOfWork.SaveChanges();
        }
    }
}