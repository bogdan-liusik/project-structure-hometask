﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.LinqQueryResults;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Entities.Enums;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class LinqQueriesService : BaseService
    {
        private readonly ICollection<Project> _projects;
        private readonly ICollection<Team> _teams;
        private readonly ICollection<User> _users;
        private readonly ICollection<Task> _tasks;
        
        public LinqQueriesService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _projects = _unitOfWork.Set<Project>().GetAll();
            _teams = _unitOfWork.Set<Team>().GetAll();
            _users = _unitOfWork.Set<User>().GetAll();
            _tasks = _unitOfWork.Set<Task>().GetAll();
        }

        public ICollection<Query1StructureDTO> Query1(int userId)
        {
            if (!IsExistUser(userId))
            {
                throw new NotFoundException(typeof(User).ToString(), userId);
            }
            
            var queryResult = _projects
                .Where(p => p.AuthorId == userId)
                .GroupJoin(_tasks,
                    p => p.Id,
                    t => t.ProjectId,
                    (project, tasks) => new
                    {
                        Project = project,
                        TasksCount = tasks.Count()
                    })
                .ToDictionary(keySelector => keySelector.Project, keySelector => keySelector.TasksCount);

            return queryResult.Select(keyValue => new Query1StructureDTO()
            {
                Project = _mapper.Map<ProjectDTO>(keyValue.Key),
                TasksCount = keyValue.Value
            }).ToList();
        }

        public ICollection<TaskDTO> Query2(int userId)
        {
            if (!IsExistUser(userId))
            {
                throw new NotFoundException(typeof(User).ToString(), userId);
            }
            
            return _tasks
                .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                .Select(task => _mapper.Map<TaskDTO>(task))
                .ToList();
        }

        public ICollection<Query3StructureDTO> Query3(int userId)
        {
            if (!IsExistUser(userId))
            {
                throw new NotFoundException(typeof(User).ToString(), userId);
            }
            
            var currentYear = DateTime.Now.Year;
            
            return _tasks
                .Where(t => t.PerformerId == userId && t.FinishedAt.Year == currentYear)
                .Select(t => new Query3StructureDTO()
                {
                    Id = t.Id,
                    Name = t.Name
                })
                .ToList();
        }

        public ICollection<Query4StructureDTO> Query4()
        {
            var currentYear = DateTime.Now.Year;

            return _teams.Select(team => new Query4StructureDTO()
            {
                TeamId = team.Id,
                TeamName = team.Name,
                Members = _users
                    .Where(u => u.TeamId == team.Id)
                    .Where(u => u.Birthday.Year < currentYear - 10)
                    .OrderByDescending(u => u.RegisteredAt)
                    .Select(u => _mapper.Map<UserDTO>(u))
                    .ToList()
            }).ToList();
        }

        public ICollection<Query5StructureDTO> Query5()
        {
            return _users
                .OrderBy(u => u.FirstName)
                .GroupJoin(_tasks,
                    u => u.Id,
                    t => t.PerformerId,
                    (user, tasks) =>
                    {
                        user.Tasks = tasks.OrderByDescending(t => t.Name.Length).ToList();
                        return user;
                    }
                )
                .Select(user => new Query5StructureDTO() 
                { 
                    User = _mapper.Map<UserDTO>(user),
                    Tasks = user.Tasks.Select(t => _mapper.Map<TaskDTO>(t)).ToList()
                })
                .ToList();
        }

        public Query6StructureDTO Query6(int userId)
        {
            if (!IsExistUser(userId))
            {
                throw new NotFoundException(typeof(User).ToString(), userId);
            }
            
            return _users
                .GroupJoin(_projects,
                    u => u.Id,
                    p => p.AuthorId,
                    (user, projects) =>
                    {
                        user.Projects = projects.ToList();
                        return user;
                    })
                .GroupJoin(_tasks,
                    u => u.Id,
                    t => t.PerformerId,
                    (user, tasks) =>
                    {
                        user.Tasks = tasks.ToList();
                        return user;
                    })
                .Where(u => u.Id == userId)
                .Select(user => new Query6StructureDTO()
                {
                    User = _mapper.Map<UserDTO>(user),
                    LastProject = _mapper.Map<ProjectDTO>(user.Projects?
                        .GroupJoin(_tasks,
                            p => p.Id,
                            t => t.ProjectId,
                            (project, tasks) =>
                            {
                                project.Tasks = tasks.ToList();
                                return project;
                            })
                        .OrderBy(p => p.CreatedAt).LastOrDefault()),
                    NumberOfTasksInLastProject = user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault().Tasks.Count,
                    IncompleteOrCancelledCount =  user.Tasks.Count(t => t.FinishedAt == null || t.State == TaskState.Canceled),
                    LongestTaskByDate = _mapper.Map<TaskDTO>(user.Tasks?.OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault())
                })
                .FirstOrDefault();
        }

        public ICollection<Query7StructureDTO> Query7()
        {
            return _projects.GroupJoin(_tasks,
                    p => p.Id,
                    t => t.ProjectId,
                    (project, tasks) =>
                    {
                        project.Tasks = tasks.ToList();
                        return project;
                    })
                .Join(_teams,
                    p => p.TeamId,
                    t => t.Id,
                    (project, team) =>
                    {
                        project.Team = team;
                        return project;
                    })
                .GroupJoin(_users,
                    p => p.TeamId,
                    u => u.TeamId,
                    (project, users) =>
                    {
                        project.Team.Users = users.ToList();
                        return project;
                    })
                .Select(project => new Query7StructureDTO()
                {
                    Project = _mapper.Map<ProjectDTO>(project),
                    LongestTaskByDescription = _mapper.Map<TaskDTO>(project.Tasks.OrderBy(t => t.Description.Length).LastOrDefault()),
                    ShortestTaskByName = _mapper.Map<TaskDTO>(project.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                    TeamMembersCountByCondition = project.Description.Length > 20 || project.Tasks.Count() < 3 ? project.Team.Users.Count() : default
                })
                .ToList();
        }
        
        private bool IsExistUser(int userId)
        {
            return _users.FirstOrDefault(u => u.Id == userId) != null;
        }
    }
}