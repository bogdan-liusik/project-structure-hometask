﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IRepository<User> _usersRepository;
        
        public UserService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _usersRepository = _unitOfWork.Set<User>();
        }

        public ICollection<UserDTO> GetAllUsers()
        {
            return _usersRepository.GetAll().Select(user => _mapper.Map<UserDTO>(user)).ToList();
        }

        public UserDTO GetUserById(int id)
        {
            var user = _usersRepository.Get(id);

            if (user == null)
            {
                throw new NotFoundException(typeof(User).ToString(), id);
            }

            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO CreateUser(UserCreateDTO userCreateDto)
        {
            var createdUser = _usersRepository.Create(_mapper.Map<User>(userCreateDto));
            _unitOfWork.SaveChanges();
            return _mapper.Map<UserDTO>(createdUser);
        }

        public UserDTO UpdateUser(UserUpdateDTO userUpdateDto)
        {
            var userExists = _usersRepository.GetAll().Any(u => u.Id == userUpdateDto.Id);

            if (!userExists)
            {
                throw new NotFoundException(typeof(User).ToString(), userUpdateDto.Id);
            }

            var updatedUser = _usersRepository.Update(_mapper.Map<User>(userUpdateDto));
            _unitOfWork.SaveChanges();
            return _mapper.Map<UserDTO>(updatedUser);
        }

        public void DeleteUser(int id)
        {
            if (_usersRepository.Get(id) == null)
            {
                throw new NotFoundException(typeof(User).ToString(), id);
            }
            
            _usersRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }
    }
}