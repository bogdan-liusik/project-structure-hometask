﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        private readonly IRepository<Team> _teamsRepository;
        
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _teamsRepository = unitOfWork.Set<Team>();
        }
        
        public ICollection<TeamDTO> GetAllTeams()
        {
            return _teamsRepository.GetAll().Select(project => _mapper.Map<TeamDTO>(project)).ToList();
        }

        public TeamDTO GetTeamById(int id)
        {
            var team = _teamsRepository.Get(id);

            if (team == null)
            {
                throw new NotFoundException(typeof(Team).ToString(), id);
            }

            return _mapper.Map<TeamDTO>(team);
        }

        public TeamDTO CreateTeam(TeamCreateDTO teamCreateDto)
        {
            var createdTeam = _teamsRepository.Create(_mapper.Map<Team>(teamCreateDto));
            _unitOfWork.SaveChanges();
            return _mapper.Map<TeamDTO>(createdTeam);
        }

        public TeamDTO UpdateTeam(TeamUpdateDTO teamUpdateDto)
        {
            var teamExists = _teamsRepository.GetAll().Any(t => t.Id == teamUpdateDto.Id);
                
            if (!teamExists)
            {
                throw new NotFoundException(typeof(Team).ToString(), teamUpdateDto.Id);
            }
            
            var updatedTeam = _teamsRepository.Update(_mapper.Map<Team>(teamUpdateDto));
            _unitOfWork.SaveChanges();
            return _mapper.Map<TeamDTO>(updatedTeam);
        }

        public void DeleteTeam(int id)
        {
            if (_teamsRepository.Get(id) == null)
            {
                throw new NotFoundException(typeof(Team).ToString(), id);
            }
            
            _teamsRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }
    }
}