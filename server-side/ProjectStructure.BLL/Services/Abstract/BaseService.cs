﻿using AutoMapper;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly IMapper _mapper;
        private protected readonly IUnitOfWork _unitOfWork;

        public BaseService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
    }
}