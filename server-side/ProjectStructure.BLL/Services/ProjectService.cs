﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IRepository<Project> _projectsRepository;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _projectsRepository = _unitOfWork.Set<Project>();
        }
        
        public ICollection<ProjectDTO> GetAllProjects()
        {
            return _projectsRepository.GetAll().Select(project => _mapper.Map<ProjectDTO>(project)).ToList();
        }

        public ProjectDTO GetProjectById(int id)
        {
            var project = _projectsRepository.Get(id);
            
            if (project == null)
            {
                throw new NotFoundException(typeof(Project).ToString(), id);
            }
            
            return _mapper.Map<ProjectDTO>(project);
        }
        
        public ProjectDTO CreateProject(ProjectCreateDTO projectCreateDto)
        {
            var createdProject = _projectsRepository.Create(_mapper.Map<Project>(projectCreateDto));
            _unitOfWork.SaveChanges();
            
            return _mapper.Map<ProjectDTO>(createdProject);
        }

        public ProjectDTO UpdateProject(ProjectUpdateDTO projectUpdateDto)
        {
            var projectExists = _projectsRepository.GetAll().Any(p => p.Id == projectUpdateDto.Id);

            if (!projectExists)
            {
                throw new NotFoundException(typeof(Project).ToString(), projectUpdateDto.Id);
            }
            
            var updatedProject = _projectsRepository.Update(_mapper.Map<Project>(projectUpdateDto));
            _unitOfWork.SaveChanges();
            
            return _mapper.Map<ProjectDTO>(updatedProject);
        }
        
        public void DeleteProject(int id)
        {
            if (_projectsRepository.Get(id) == null)
            {
                throw new NotFoundException(typeof(Project).ToString(), id);
            }
            
            _projectsRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }
    }
}