﻿using AutoMapper;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
            
            CreateMap<Project, ProjectCreateDTO>();
            CreateMap<ProjectCreateDTO, Project>();
            
            CreateMap<Project, ProjectUpdateDTO>();
            CreateMap<ProjectUpdateDTO, Project>();
        }
    }
}