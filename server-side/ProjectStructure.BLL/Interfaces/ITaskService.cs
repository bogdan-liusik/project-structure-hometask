﻿using System.Collections.Generic;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        ICollection<TaskDTO> GetAllTasks();
        TaskDTO GetTaskById(int projectId);
        TaskDTO CreateTask(TaskCreateDTO project);
        TaskDTO UpdateTask(TaskUpdateDTO project);
        void DeleteTask(int projectId);
    }
}