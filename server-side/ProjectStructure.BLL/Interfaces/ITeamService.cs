﻿using System.Collections.Generic;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        ICollection<TeamDTO> GetAllTeams();
        TeamDTO GetTeamById(int projectId);
        TeamDTO CreateTeam(TeamCreateDTO project);
        TeamDTO UpdateTeam(TeamUpdateDTO project);
        void DeleteTeam(int projectId);
    }
}