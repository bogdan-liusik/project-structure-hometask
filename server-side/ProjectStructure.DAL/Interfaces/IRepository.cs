﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity: class
    {
        List<TEntity> GetAll(Func<TEntity,bool> filter = null);
        TEntity Get(int id);
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entityToUpdate);
        void Delete(int id);
    }
}