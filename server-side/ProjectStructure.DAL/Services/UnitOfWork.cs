﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Entities.Abstract;
using ProjectStructure.DAL.Interfaces;
using Task = ProjectStructure.DAL.Entities.Task;

namespace ProjectStructure.DAL.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public UnitOfWork(DataContext context)
        {
            _context = context;
        }
        
        public IRepository<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return new Repository<TEntity>(GetCollection<TEntity>());
        }

        public bool SaveChanges()
        {
            return true;
        }
        
        public async Task<bool> SaveChangesAsync()
        {
            return true;
        }
        
        private List<TEntity> GetCollection<TEntity>()
        {
            var entityType = typeof(TEntity);
            
            var typeMatching = new Dictionary<Type, List<TEntity>>()
            {
                {typeof(Project), _context.Projects as List<TEntity>},
                {typeof(Task), _context.Tasks as List<TEntity>},
                {typeof(Team), _context.Teams as List<TEntity>},
                {typeof(User), _context.Users as List<TEntity>}
            };
            
            return typeMatching[entityType];
        }
    }
}