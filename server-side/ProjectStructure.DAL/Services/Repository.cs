﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Entities.Abstract;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.DAL.Services
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity: BaseEntity
    {
        private readonly List<TEntity> _collection;

        public Repository(List<TEntity> collection)
        {
            _collection = collection;
        }

        public List<TEntity> GetAll(Func<TEntity, bool> filter = null)
        {
            return filter == null ? _collection : _collection.Where(filter).ToList();
        }
        
        public TEntity Get(int id)
        {
            return _collection.SingleOrDefault(e => e.Id == id);
        }

        public TEntity Create(TEntity entity)
        {
            var lastEntityId = _collection.LastOrDefault()?.Id ?? 0;
            entity.Id = lastEntityId + 1;
            _collection.Add(entity);
            return entity;
        }
        
        public TEntity Update(TEntity entityToUpdate)
        {
            //EntityFramework will make everything look much better :)
            var entity = _collection.SingleOrDefault(e => e.Id == entityToUpdate.Id);
            
            _collection.Remove(entity);
            _collection.Insert(entityToUpdate.Id - 1, entityToUpdate);
            
            return _collection.SingleOrDefault(e => e.Id == entityToUpdate.Id);
        }
        
        public void Delete(int id)
        {
            var entity = _collection.SingleOrDefault(e => e.Id == id);
            _collection.Remove(entity);
        }
    }
}