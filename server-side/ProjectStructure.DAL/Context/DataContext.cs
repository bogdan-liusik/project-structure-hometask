﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Entities.Enums;

namespace ProjectStructure.DAL.Context
{
    public class DataContext
    {
        public DataContext()
        {
            Seed();
        }
        
        public ICollection<Project> Projects { get; private set; }
        public ICollection<Task> Tasks { get; private set; }
        public ICollection<Team> Teams { get; private set; }
        public ICollection<User> Users { get; private set; }
        
        private void Seed()
        {
            Teams = GenerateRandomTeams();
            Users = GenerateRandomUsers(Teams);
            Projects = GenerateRandomProjects(Users, Teams);
            Tasks = GenerateRandomTasks(Users, Projects);
        }
        
        private ICollection<Team> GenerateRandomTeams()
        {
            var teamId = 0;

            var teamsFake = new Faker<Team>()
                .RuleFor(t => t.Id, f => teamId++)
                .RuleFor(t => t.Name, f => f.Name.FullName())
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new DateTime(2018, 1, 1), DateTime.Now.AddMonths(-1)));

            return teamsFake.Generate(10);
        }

        private ICollection<Project> GenerateRandomProjects(ICollection<User> users, ICollection<Team> teams)
        {
            var projectId = 0;
            var createdAt = new DateTime();

            var projectsFake = new Faker<Project>()
                .RuleFor(p => p.Id, f => projectId++)
                .RuleFor(p => p.Name, p => p.Commerce.ProductName())
                .RuleFor(p => p.Description, f => f.Commerce.ProductDescription())
                .RuleFor(p => p.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(p => p.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(p => p.CreatedAt, f =>
                {
                    createdAt = f.Date.Between(new DateTime(2018, 1, 1), DateTime.Now.AddDays(-5));
                    return createdAt;
                })
                .RuleFor(p => p.Deadline, f => f.Date.Between(createdAt, DateTime.Now.AddMonths(5)));

            return projectsFake.Generate(100);
        }

        private ICollection<User> GenerateRandomUsers(ICollection<Team> teams)
        {
            var userId = 0;
            var userBirthDay = new DateTime();

            var usersFake = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                .RuleFor(u => u.LastName, f => f.Name.LastName())
                .RuleFor(u => u.Email, u => u.Internet.Email())
                .RuleFor(u => u.Birthday, f =>
                {
                    userBirthDay = f.Date.Between(new DateTime(1970, 1, 1), DateTime.Now.AddYears(-7));
                    return userBirthDay;
                })
                .RuleFor(u => u.RegisteredAt, f => f.Date.Between(userBirthDay.AddYears(8), DateTime.Now))
                .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id);

            return usersFake.Generate(130);
        }

        private ICollection<Task> GenerateRandomTasks(ICollection<User> users, ICollection<Project> projects)
        {
            int taskId = 0;
            DateTime createdAt;

            var tasksFake = new Faker<Task>()
                .RuleFor(t => t.Id, f => taskId++)
                .RuleFor(t => t.Name, f => f.Lorem.Sentence(2, 3))
                .RuleFor(t => t.Description, f => f.Lorem.Sentence(6, 8))
                .RuleFor(t => t.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(t => t.State,
                    f => f.PickRandom(TaskState.Canceled, TaskState.Done, TaskState.InProgress, TaskState.ToDo))
                .RuleFor(t => t.CreatedAt, (f, t) =>
                {
                    var project = Projects.FirstOrDefault(p => p.Id == t.ProjectId);
                    createdAt = f.Date.Between(project.CreatedAt, project.Deadline.AddDays(-1));
                    return createdAt;
                })
                .RuleFor(t => t.FinishedAt, (f, t) =>
                {
                    var project = Projects.FirstOrDefault(p => p.Id == t.ProjectId);
                    return f.Date.Between(t.CreatedAt, project.Deadline);
                })
                .RuleFor(t => t.PerformerId, f => f.PickRandom(users).Id);
            
            return tasksFake.Generate(500);
        }
    }
}