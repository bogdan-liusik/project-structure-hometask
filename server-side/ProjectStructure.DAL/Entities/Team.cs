﻿using System;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.DAL.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<User> Users { get; set; }
    }
}