﻿namespace ProjectStructure.DAL.Entities.Enums
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}