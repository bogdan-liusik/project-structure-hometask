﻿using System;
using ProjectStructure.DAL.Entities.Abstract;
using ProjectStructure.DAL.Entities.Enums;

namespace ProjectStructure.DAL.Entities
{
    public class Task : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
    }
}