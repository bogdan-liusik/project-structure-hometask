﻿namespace ProjectStructure.DAL.Entities.Abstract
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}