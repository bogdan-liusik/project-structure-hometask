﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        
        [HttpGet]
        public ActionResult<ICollection<TeamDTO>> GetAllTeams()
        {
            return Ok(_teamService.GetAllTeams());
        }

        [HttpGet("{id:int}")]
        public ActionResult<TeamDTO> GetTeamById(int id)
        {
            try
            {
                return Ok(_teamService.GetTeamById(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult<TeamDTO> CreateTeam([FromBody] TeamCreateDTO teamCreateDto)
        {
            try
            {
                return Ok(_teamService.CreateTeam(teamCreateDto));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<TeamDTO> UpdateTeam([FromBody] TeamUpdateDTO teamUpdateDto)
        {
            try
            {
                return Ok(_teamService.UpdateTeam(teamUpdateDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteTeam(int id)
        {
            try
            {
                _teamService.DeleteTeam(id);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}