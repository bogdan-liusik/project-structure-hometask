﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        
        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> GetAllProjects()
        {
            return Ok(_projectService.GetAllProjects());
        }
        
        [HttpGet("{id:int}")]
        public ActionResult<ProjectDTO> GetProjectById(int id)
        {
            try
            {
                return Ok(_projectService.GetProjectById(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpPost]
        public ActionResult<ProjectDTO> CreateProject([FromBody] ProjectCreateDTO projectCreateDto)
        {
            try
            {
                return Ok(_projectService.CreateProject(projectCreateDto));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<ProjectDTO> UpdateProject([FromBody] ProjectUpdateDTO projectUpdateDto)
        {
            try
            {
                return Ok(_projectService.UpdateProject(projectUpdateDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteProject(int id)
        {
            try
            {
                _projectService.DeleteProject(id);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}