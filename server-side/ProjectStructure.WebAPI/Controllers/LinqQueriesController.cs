﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTO.LinqQueryResults;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqQueriesController : ControllerBase
    {
        private readonly LinqQueriesService _linqQueriesService;
        
        public LinqQueriesController(LinqQueriesService linqQueriesService)
        {
            _linqQueriesService = linqQueriesService;
        }

        [HttpGet("query1/{id:int}")]
        public ActionResult<ICollection<Query1StructureDTO>> Query1(int id)
        {
            try
            {
                return Ok(_linqQueriesService.Query1(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpGet("query2/{id:int}")]
        public ActionResult<ICollection<TaskDTO>> Query2(int id)
        {
            try
            {
                return Ok(_linqQueriesService.Query2(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("query3/{id:int}")]
        public ActionResult<ICollection<Query3StructureDTO>> Query3(int id)
        {
            try
            {
                return Ok(_linqQueriesService.Query3(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("query4")]
        public ActionResult<ICollection<Query4StructureDTO>> Query4()
        {
            try
            {
                return Ok(_linqQueriesService.Query4());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("query5")]
        public ActionResult<ICollection<Query5StructureDTO>> Query5()
        {
            try
            {
                return Ok(_linqQueriesService.Query5());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("query6/{id:int}")]
        public ActionResult<Query6StructureDTO> Query6(int id)
        {
            try
            {
                return Ok(_linqQueriesService.Query6(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("query7")]
        public ActionResult<ICollection<Query7StructureDTO>> Query7()
        {
            try
            {
                return Ok(_linqQueriesService.Query7());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}