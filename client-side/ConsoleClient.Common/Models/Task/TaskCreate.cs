﻿using System;

namespace ConsoleClient.Common.Models.Task
{
    public class TaskCreate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime FinishedAt { get; set; }
    }
}