﻿namespace ConsoleClient.Common.Models.Task
{
    public class TaskUpdate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}