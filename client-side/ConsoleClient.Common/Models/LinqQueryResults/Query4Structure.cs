﻿using System.Collections.Generic;
using ConsoleClient.Common.Models.User;

namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query4Structure
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public ICollection<User.User> Members { get; set; }
    }
}