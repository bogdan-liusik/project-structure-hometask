﻿using ConsoleClient.Common.Models.Task;
using ConsoleClient.Common.Models.Project;
using ConsoleClient.Common.Models.User;

namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query6Structure
    {
        public User.User User { get; set; }
        public Project.Project LastProject { get; set; }
        public int NumberOfTasksInLastProject { get; set; }
        public int IncompleteOrCancelledCount { get; set; }
        public Task.Task LongestTaskByDate { get; set; }
    }
}