﻿using System.Collections.Generic;

namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query5Structure
    {
        public User.User User { get; set; }
        public ICollection<Task.Task> Tasks { get; set; }
    }
}