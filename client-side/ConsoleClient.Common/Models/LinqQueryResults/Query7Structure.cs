﻿namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query7Structure
    {
        public Project.Project Project { get; set; }
        public Task.Task LongestTaskByDescription { get; set; }
        public Task.Task ShortestTaskByName { get; set; }
        public int TeamMembersCountByCondition { get; set; }
    }
}