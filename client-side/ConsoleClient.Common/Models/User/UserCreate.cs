﻿using System;

namespace ConsoleClient.Common.Models.User
{
    public class UserCreate
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime RegisteredAd { get; set; } = DateTime.Now;
    }
}