﻿using System;

namespace ConsoleClient.Common.Models.Project
{
    public class ProjectUpdate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}