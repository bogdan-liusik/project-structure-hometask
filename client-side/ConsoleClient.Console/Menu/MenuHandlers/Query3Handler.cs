﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers
{
    using System;
    public class Query3Handler : LinqQueriesHandler
    {
        public Query3Handler(LinqQueriesService queryService) : base(queryService) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the list (id, name) from the collection of tasks that are finished in the current year (2021) for a particular user (by id).", ConsoleColor.Yellow);
                
                Console.WriteLine("Enter user id: ");
                string id = Console.ReadLine();
                
                var results = await QueryService.Query3(Int32.Parse(id));
                
                if (results.Count == 0)
                {
                    Console.WriteLine("List is empty for current user.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var result in results)
                {
                    WriteSuccessLine($"ID: {result.Id} | Name: {result.Name}");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}