﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers
{
    using System;
    public class Query7Handler : LinqQueriesHandler
    {
        public Query7Handler(LinqQueriesService queryService) : base(queryService) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the following structures: \n" +
                               "Project\n" +
                               "The longest project task (by description)\n" +
                               "The shortest project task (by name)\n" +
                               "Total number of users in the project team, where either the project description > 20 characters or the number of tasks < 3", ConsoleColor.Yellow);

                var results = await QueryService.Query7();

                foreach (var result in results)
                {
                    WriteSuccessLine($"Project ID: {result.Project.Id} | Project Name: {result.Project.Name}");
                    WriteSuccessLine($"The longest project task (by description): \n" +
                                     $"Task ID: {result?.LongestTaskByDescription.Id} | Task description length: {result?.LongestTaskByDescription.Description.Length}");
                    WriteSuccessLine($"The shortest project task (by name): \n" +
                                     $"Task ID: {result?.ShortestTaskByName.Id} | Task name length: {result?.ShortestTaskByName.Name.Length}");
                    WriteSuccessLine($"Total number of users in the project team: {result?.TeamMembersCountByCondition}\n");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}