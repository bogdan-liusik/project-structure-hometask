﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers
{
    using System;
    public class Query4Handler : LinqQueriesHandler
    {
        public Query4Handler(LinqQueriesService queryService) : base(queryService) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get a list (id, team name, and user list) of teams whose members are over 10 years old, sorted by user registration date in descending order, and grouped by team.", ConsoleColor.Yellow);

                var results = await QueryService.Query4();
        
                if (results.Count == 0)
                {
                    Console.WriteLine("List is empty.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var result in results)
                {
                    WriteColorLine($"Command ID: {result.TeamId} | Command Name: {result.TeamName}", ConsoleColor.Cyan);
                    foreach (var user in result.Members)
                    {
                        WriteSuccessLine($"User ID: {user.Id} | First name {user.FirstName} | Team ID: {user.TeamId}");
                    }
                }
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}