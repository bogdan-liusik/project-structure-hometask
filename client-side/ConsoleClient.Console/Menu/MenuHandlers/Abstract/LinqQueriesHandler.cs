﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;

namespace ConsoleClient.Console._Menu.MenuHandlers.Abstract
{
    public abstract class LinqQueriesHandler
    {
        private protected readonly LinqQueriesService QueryService;
            
        public LinqQueriesHandler(LinqQueriesService queryService)
        {
            QueryService = queryService;
        }

        public abstract Task Handle();
    }
}