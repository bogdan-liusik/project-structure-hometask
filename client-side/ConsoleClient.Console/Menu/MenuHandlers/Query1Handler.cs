﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers
{
    using System;
    public class Query1Handler : LinqQueriesHandler
    {
        public Query1Handler(LinqQueriesService queryService) : base(queryService) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the number of tasks of a particular user's project (by id) (dictionary, where the key is the project, and the value is the number of tasks).", ConsoleColor.Yellow);
                
                Console.WriteLine("Enter user id: ");
                string id = Console.ReadLine();
                
                var result = await QueryService.Query1(Int32.Parse(id));

                if (result.Count == 0)
                {
                    Console.WriteLine("This user has no projects of his own.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var keyValuePair in result)
                {
                    WriteSuccessLine($"ProjectID: {keyValuePair.Key.Id} | ProjectName: {keyValuePair.Key.Name} | Number of tasks: {keyValuePair.Value}");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}