﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;

namespace ConsoleClient.Console._Menu
{
    using static MenuHelper;
    
     public class Menu
     {
         private bool _running = true;
         readonly Dictionary<int, LinqQueriesHandler> _handlers;
    
         public Menu(LinqQueriesService linqQueriesService)
         {
             _handlers = new Dictionary<int, LinqQueriesHandler>()
             {
                 {1, new Query1Handler(linqQueriesService)},
                 {2, new Query2Handler(linqQueriesService)},
                 {3, new Query3Handler(linqQueriesService)},
                 {4, new Query4Handler(linqQueriesService)},
                 {5, new Query5Handler(linqQueriesService)},
                 {6, new Query6Handler(linqQueriesService)},
                 {7, new Query7Handler(linqQueriesService)},
             };
         }
         
         public void Start()
         {
             while (_running)
             {
                 ShowCommands();
                 ChooseCommand().Wait();
             }
         }
         
         private void ShowCommands()
         {
             WriteColorLine("┌" + new string('─', 13) + "QUERY SERVICE MENU" + new string('─', 11) + "┐", ConsoleColor.Green);
             System.Console.ForegroundColor = ConsoleColor.Cyan;
             System.Console.WriteLine("| 1. Result of Query 1                     |");
             System.Console.WriteLine("| 2. Result of Query 2                     |");
             System.Console.WriteLine("| 3. Result of Query 3                     |");
             System.Console.WriteLine("| 4. Result of Query 4                     |");
             System.Console.WriteLine("| 5. Result of Query 5                     |");
             System.Console.WriteLine("| 6. Result of Query 6                     |");
             System.Console.WriteLine("| 7. Result of Query 7                     |");
             WriteColorLine("| 0. Exit                                  |", ConsoleColor.Red);
             WriteColorLine("└" + new string('─', 42) + "┘", ConsoleColor.Green);
         }
    
         private async Task ChooseCommand()
         {
             try
             {
                 System.Console.Write("Enter your choice: ");
                 
                 if(Int32.TryParse(System.Console.ReadLine(), out var menuItem))
                 {
                     if (menuItem is <= 7 and > 0)
                     {
                         await _handlers[menuItem].Handle();
                     }
                     else if(menuItem == 0)
                     {
                         _running = false;
                     }
                     else
                     {
                         throw new InvalidOperationException("Item number must be in range 1 - 7! Try Again.");
                     }
                 }
                 else
                 {
                     throw new InvalidOperationException("Invalid character! Try Again.");
                 }
             }
             catch (InvalidOperationException ex)
             {
                 WriteErrorLine(ex.Message);
             }
    
             WriteColorLine("\n" + new string('─', 81) + "\n", ConsoleColor.DarkGray);
         }
    }
}