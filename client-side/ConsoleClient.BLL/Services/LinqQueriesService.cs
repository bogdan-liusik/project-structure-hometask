﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsoleClient.Common.Models.LinqQueryResults;
using ConsoleClient.Common.Models.Project;
using Task = ConsoleClient.Common.Models.Task.Task;

namespace ConsoleClient.BLL.Services
{
    public class LinqQueriesService
    {
        private readonly HttpClientService _httpClientService;

        public LinqQueriesService(HttpClientService httpClientService)
        {
            _httpClientService = httpClientService;
        }

        public async Task<Dictionary<Project, int>> Query1(int id)
        {
            var result = await _httpClientService.FetchListObjectsAsync<Query1Structure>($"query1/{id}");
            return result.ToDictionary(r => r.Project, r => r.TasksCount);
        }
            
        public async Task<List<Task>> Query2(int id)
        {
            return await _httpClientService.FetchListObjectsAsync<Task>($"query2/{id}");
        }

        public async Task<List<Query3Structure>> Query3(int id)
        {
            return await _httpClientService.FetchListObjectsAsync<Query3Structure>($"query3/{id}");
        }

        public async Task<List<Query4Structure>> Query4()
        {
            return await _httpClientService.FetchListObjectsAsync<Query4Structure>($"query4");
        }

        public async Task<List<Query5Structure>> Query5()
        {
            return await _httpClientService.FetchListObjectsAsync<Query5Structure>($"query5");
        }

        public async Task<Query6Structure> Query6(int id)
        {
            return await _httpClientService.FetchObjectAsync<Query6Structure>($"query6/{id}");
        }

        public async Task<List<Query7Structure>> Query7()
        {
            return await _httpClientService.FetchListObjectsAsync<Query7Structure>($"query7");
        }
    }
}